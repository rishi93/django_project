FROM python:3
ARG MAX_WORKERS
ENV MAX_WORKERS=${MAX_WORKERS}
WORKDIR /app
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
COPY . .
EXPOSE 80
CMD python manage.py runserver 0.0.0.0:80