from django.urls import path
from ninja import NinjaAPI

from . import views

api = NinjaAPI()

@api.get("/")
def get_root(request):
    return {"message": "Hello world!"}

urlpatterns = [
    path("", views.index, name="index"),
    path("api/", api.urls)
]